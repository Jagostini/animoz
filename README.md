# Upload une image docker sur un registry

## Etape 1 - Connexion

Dans un terminal, saisir la commande suivante

```
docker login registry.gitlab.com
```

On vous demande, votre identifiant et mot de passe gitlab la premiere fois.

## Etape 2 - Build image

Construire l'image avec un nom de référence pour le registry gitlab

option 1 : La commande suivante est le nom minimal :

```
docker build -t registry.gitlab.com/<workspace>/<nameproject> .
```

option 2 : préciser un Tag

```
docker build -t registry.gitlab.com/<workspace>/<nameproject>:<tag> .
```

## Etape 3 - Push

Il suffit de mettre le nom de l'image construite, preciser le tag si vous avez pris l'option2

```
docker push <nameimage>:<tag>
```

# Docker autres commandes

Voir les images local

```
docker image ls
```

voir les containers actif

```
docker ps
```

voir les containers arreté

```
docker ps -a
```

afficher que les ID de tous les containers

```
docker ps -aq
```

Supprimer tous les containers

```
docker rm $(docker ps -aq)
```

Demarrer un container

```
docker run <nameimage>
```

l'option -d permet de detacher le container
l'option -p permet de preciser le port ex: -p 80:80
l'option -e permet de preciser des variables d'environment
l'option -v permet de preciser les volumes


Suite a un demarrage pour entrer dans le container

```
docker exec -it <nameimage> <commande>

# example
docker exec -it ubuntu bash
```

l'option -it permet de rentrer en mode interactif

Telecharger une image

```
docker pull <nameimage>
```

Voir les volumes créés

```
docker volume ls
```

Suprimer des volumes

```
docker volume rm <idvolume>
```

voir le réseau docker

```
docker network ls
```

voir les info d'un container

```
docker inspect <namecontainer>
```

# docker-compose

Demarer tous les services d'un docker-compose

```
docker-compose up
```

l'option -d permet de detacher le compose

Arreter tous les services d'un docker-compose

```
docker-compose down
```

l'option -v permet de nettoyer les volumes

Telecharger les dernieres images des services

```
docker-compose pull
```

Vous pouvez également utiliser qu'un seul service en indiquant le nom

```
docker-compose up -d <nameservice>
```

# SSH creation

## Generation de la clé

Saisir la commande suivante dans un terminal :

```
ssh-keygen -t ed_25519 -b 4096 -C "votrenom_key"
```

Faire `entrer` pour conserver le chemin et le nom par defaut

Vous avez la possibilité d'ajouter un mot de passe sur votre clé ssh, faire `entrer` si vous ne voulez pas en mettre, puis encore `entrer` pour confirmer

## composition de la clé

Dans votre répertoire utilisateur, vous avez un repertoire caché .ssh dans lequel vous avez un fichier .pub

Le fichier `id_ed25519.pub` est votre clé publique, contenu que vous pouvez partager

l'autre fichier `id_ed25519` est votre clé privée, à conserver secret

## Configuratin personalisé

Ajouter un fichier `config` dans le dossier `.ssh`

```
host gitlab                         # le nom que vous souaitez
  Hostname gitlab.com               # l'adresse du service, ex: 10.5.26.54 ou mondomaine.com
  User votreuser                    # l'utilisateru utilisé pour se connecter
  IdentityFile ~/.ssh/id_ed25519    # la reference à la clé utilisé
```

# Bonus

## Utilisation Du projet Jib

Le projet à pour but de remplacer le fichier Dockerfile pour n'utiliser que le pom.xml et utiliser les commandes maven.

Ci-dessous le lien du projet pour voir la documentation :

https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin

il suffit d'ajouter ce contenu dans votre fichier pom.xml et comme configuré ci-dessous lors du `mvn package` la génération d'une image docker sera effectué

```xml
<plugin>
    <groupId>com.google.cloud.tools</groupId>
    <artifactId>jib-maven-plugin</artifactId>
    <version>2.2.0</version>
    <executions>
        <execution>
            <phase>package</phase>
            <goals>
                <goal>dockerBuild</goal>
            </goals>
        </execution>
    </executions>
    <configuration>
        <from>
            <image>openjdk:10-jre-slim</image>
        </from>
        <to>
            <image>registry.gitlab.com/votreuser/votrenomprojet</image>
            <tags>
                <tag>0.1.0</tag>
                <tag>latest</tag>
            </tags>
        </to>
        <container>
            <jvmFlags>
                <jvmFlag>-Xdebug</jvmFlag>
            </jvmFlags>
            <ports>
                <port>8080</port>
            </ports>
            <format>OCI</format>
        </container>
    </configuration>
</plugin>
```

## Lien projet Hadolint

https://github.com/hadolint/hadolint

L'exemple est dans le fichier gitlab-ci

## lien projet Trivy

https://github.com/aquasecurity/trivy

L'exemple est dans le fichier gitlab-ci