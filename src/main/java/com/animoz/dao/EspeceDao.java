package com.animoz.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.animoz.modele.Espece;

@Repository
public class EspeceDao {
	
	@PersistenceContext
	private EntityManager em;
	
	public List<Espece> getEspeces() {
		return em.createQuery("select e from Espece e order by e.nom", Espece.class).getResultList();
	}

	public boolean existe(String nomEspece) {
		long nb = em.createQuery("select count(e) from Espece e where lower(e.nom) = lower(:nom)", Long.class)
				    .setParameter("nom", nomEspece)
				    .getSingleResult();
		return nb > 0;
	}

	public void ajouter(Espece espece) {
		em.persist(espece);
	}

}
